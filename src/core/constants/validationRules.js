const REGEXP_EMAIL = /^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$/;
const REGEXP_NUMBER = /^\d+$/;

// Positive testing "is value valid?": if value matches rule, return true.
const RULES = {
	required: value => value && value.length,
	minLength3: value => value && value.length > 2,
	number: value => REGEXP_NUMBER.test(value),
	greaterThanZero: value => REGEXP_NUMBER.test(value) && value > 0,
	checked: value => value === true,
	unchecked: value => value === false,
	hasValues: value => value && Object.values(value).length,
	email: value => REGEXP_EMAIL.test(value),
	isFile: value => value && typeof value.name === 'string',
	filledArray: value => Array.isArray(value) && value.length > 0,
};

export default RULES;
