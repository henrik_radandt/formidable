import {debug, defaultProps, compose, ensureArray, getContext, lifecycle, mapProps, merge, renameProp, withProps, withHandlers} from '../../utils';
import {expose} from '../../core/utils';

import React from 'react';
import PropTypes from 'prop-types';

const API = {
	getValue: PropTypes.func.isRequired,
	findValue: PropTypes.func.isRequired,
	update: PropTypes.func.isRequired,
	name: PropTypes.string,

	add: PropTypes.func,
	remove: PropTypes.func,
};

export default compose(
	getContext(API),

	defaultProps({
		min: 0,
		className: '',
	}),

	withHandlers({
		buildIndex: props => () => `${props.name}_${Math.random()}`,
		findIndex: props => __id => props.getValue().findIndex(item => item.__id === __id),
	}),

	withHandlers({
		add: props => value => {
			const items = props.getValue() || [];

			if (!props.max || (items.length < props.max)) {
				props.update([...items].concat({__id: props.buildIndex()}))
			}
		},

		remove: props => (id, min) => {
			const items = [...props.getValue()];

			let minCheckValue = min === undefined ? props.min : min;

			if (minCheckValue === undefined || items.length > minCheckValue) {
				items.splice(props.findIndex(id), 1);
				props.update(items);
			}
		},

		update: props => (value, id) => {
			const items = [...props.getValue()];
			const index = props.findIndex(id);
			const updatedItem = Object.assign({}, items[index], value);

			items.splice(index, 1, updatedItem);
			props.update(items);
		},

		findValue: props => (path, id) => props.findValue(path, id),
	}),

	lifecycle({
		componentDidMount() {
			// Override default InitialValue-push.
			const {initialValue, min, getValue, add} = this.props;

			// If initial value is defined, we will commit it to store on mount.
			// We have to add, to generate a possible missing __id.
			initialValue !== undefined &&
			getValue() === undefined &&
			ensureArray(initialValue).map(add);

			const items = getValue();

			if (items) {
				for (let i = items.length; i < min; i += 1) {
					add();
				}
			}
		},
	}),

	expose(API),

	mapProps(({getValue, findValue, add, remove, name, ...props}) => props),

);
