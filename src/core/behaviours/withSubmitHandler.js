import {expose} from '../utils';
import {debug, clean, compose, defaultProps, getContext, mapProps, withProps, withStateHandlers} from '../../utils';
import React from 'react';
import PropTypes from 'prop-types';

const API = {
	submit: PropTypes.func,
	cleaner: PropTypes.object,
	isSubmitting: PropTypes.bool,
	isSubmitted: PropTypes.bool,
};

export default compose(
	getContext({
		validate: PropTypes.func.isRequired,
		getValue: PropTypes.func.isRequired,
	}),

	defaultProps({
		onSubmit: data => console.log('Fallback submit.', data),
		cleaner: {},
	}),

	withStateHandlers(
		{
			isSubmitting: false,
			isSubmitted: false,
		},
		{
			update: state => update => ({...state, ...update}),
		},
	),

	withProps(props => ({
		submit: () => {
			if (props.isSubmitting) {
				return;
			}
			props.update({isSubmitting: true});

			props.validate(
				null,
				isValid => {
					// If (sub)form is valid, execute closest handler with cleaned data.
					isValid && props.onSubmit(clean(props.getValue(), props.cleaner));

					// Update internal state.
					// For the moment we will assume, that a valid form has been succesful submitted.
					window.setTimeout(
						() => props.update({
							isSubmitting: false,
							isSubmitted: isValid,
						}),
						2000
					);
				}
			);
		}
	})),

	expose(API),

	mapProps(({validate, getValue, onSubmit, submit, isSubmitting, isSubmitted, cleaner, ...props}) => props),
);
