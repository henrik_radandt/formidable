import {debug, branch, compose, getContext, mapProps, set, withProps, withStateHandlers} from '../../utils';
import {expose} from '../utils';

import PropTypes from 'prop-types';

const API = {
	addPage: PropTypes.func,
	nextPage: PropTypes.func,
	prevPage: PropTypes.func,
	toPage: PropTypes.func,
	isActivePage: PropTypes.func,
	getPages: PropTypes.func,
};

export default compose(
	withStateHandlers(
		{
			pages: [],
			currentPage: 0,
		},{
			addPage: state => ({callback, ...pageData}) => {
				callback(state.pages.length);

				return {
					...state,
					pages: state.pages.concat(pageData || {}),
				};
			},

			nextPage: state => () => ({
				...state,
				currentPage: Math.min(state.currentPage + 1, state.pages.length - 1)
			}),

			prevPage: state => () => ({
				...state,
				currentPage: Math.max(state.currentPage - 1, 0)
			}),

			toPage: state => nextPage => ({
				...state,
				currentPage: nextPage
			}),
		}
	),

	withProps(props => ({
		isActivePage: index => props.currentPage === index,
		getPages: () => ({pages: props.pages, current: props.currentPage}),
	})),

	expose(API),

	mapProps(({addPage, nextPage, prevPage, toPage, isActivePage, getPages, pages, currentPage, ...props}) => props),
);
