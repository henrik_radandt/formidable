import {expose} from '../utils';
import {debug, branch, compose, get, getContext, isPlainObject, lifecycle, mapProps, renameProp, withHandlers, withProps} from '../../utils';

import {shouldUpdate, shallowEqual} from 'recompose';

import React from 'react';
import PropTypes from 'prop-types';

const API = {
	hasValueProvider: PropTypes.bool,
	getValue: PropTypes.func,
	update: PropTypes.func,
	findValue: PropTypes.func,
	name: PropTypes.string,
};

export default compose(
	// Stash pre-fill value from Component's props.
	renameProp('value', 'initialValue'),

	getContext(API),

	// Stash parent-update function.
	//"Commit" will update closest parent and data will bubble up component-tree.
	renameProp('update', 'commit'),

	withProps(props => ({
		hasValueProvider: true,
	})),

	withHandlers({
		update: props => value => props.commit({[props.name]: value}),

		getValue: props => () => {
			const value = props.getValue();
			const askedValue = get(value, props.name);

			return askedValue;
		},

		findValue: props => (path, __id) => {
			// Look for matching key at current level.
			let value = get(props.getValue(), `${props.name}.${path}`);

			// Handle Multiplier-Array-Values.
			// Multiplier add two levels of nested datas, we have to respect.
			if (value === undefined && __id) {
				// Parent-Path is coded within asked id.
				const [subPath] = __id.split('_');
				// Look for parent of multiplier-item.
				value = get(props.getValue(), `${props.name}.${subPath}`);

				// If found an matching array, search for matching id.
				if (Array.isArray(value)) {
					value = get(value.find(item => item.__id === __id), path);
				}
			}

			// Look at parent (recursively calling bottom-up).
			if (value === undefined) {
				value = props.findValue(path, __id);
			}

			return value;
		},
	}),

	lifecycle({
		componentDidMount() {
			const {initialValue, getValue, update} = this.props;

			// If initial value is defined, we will commit it to store on mount.
			initialValue !== undefined &&
			getValue() === undefined &&
			update(initialValue);
		},
	}),

	// Consument will update API recursively for its children.
	expose(API),

	mapProps(({hasValueProvider, initialValue, getValue, name, findValue, commit, value, ...props}) => props),
);
