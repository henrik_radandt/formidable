import {expose} from '../utils';
import {debug, compose, get, getContext, mapProps, merge, mergeWith, withHandlers, withProps, withStateHandlers} from '../../utils';

import React from 'react';
import PropTypes from 'prop-types';

const API = {
	update: PropTypes.func,
	findValue: PropTypes.func,
	getValue: PropTypes.func,
};

// Handler for Array-Values.
// Arrays should always send whole updated array. State will be replaced, not merged.
const ARRAY_CUSTOMIZER = (objValue, srcValue) => {
	if (Array.isArray(srcValue)) {
		return srcValue.map((value, index) => {
			const existingValue = Array.isArray(objValue) &&
				objValue.find(({__id}) => __id === value.__id);

			return merge(existingValue || {}, value)
		});
	}
};

export default compose(
	getContext({
		name: PropTypes.string,
	}),

	// FORM-DATA
	withStateHandlers(
		props => ({
			value: {
				[props.name]: {}
			},
		}), {
			update: (state, props) => value => mergeWith({}, state, {value}, ARRAY_CUSTOMIZER),
		}
	),

	withHandlers({
		findValue: props => path => get(props.value, path),
		getValue: props => () => props.value,
	}),

	expose(API),

	mapProps(({name, update, findValue, getValue, value, ...props}) => props),
);
