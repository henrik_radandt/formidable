import {expose} from '../utils';
import {debug, branch, compose, get, getContext, mapProps, merge, set, withHandlers, withProps, withStateHandlers} from '../../utils';
import {shouldUpdate, shallowEqual} from 'recompose';
import React from 'react';
import PropTypes from 'prop-types';

const API = {
	addValidations: PropTypes.func,
	removeValidations: PropTypes.func,
	validate: PropTypes.func,
	validations: PropTypes.array,

 	getValidationResults: PropTypes.func,
	RULES: PropTypes.object,
};

const checkIsValid = ({names, values, tests, RULES, debug}) => tests.every(testName => {
	const testExists = Boolean(RULES[testName]);
	const isValid = testExists && RULES[testName].call(null, values);

	debug && console.log([
		`Validate "${testName}" on key(s) "${names.join(', ')}" with current value "${value}"`,
		testExists && `=> Value is ${isValid ? 'valid' : 'invalid'}.`,
		!testExists && `=> Test "${testName}" not found. Mark value as invalid.`
	].filter(i => i).join(' '));

	return isValid;
});

export default (RULES = {}) => compose(
	getContext({
		RULES: PropTypes.object
	}),

	withProps(props => ({
		RULES: RULES || props.RULES
	})),

	withStateHandlers(
		{
			validations: [],
		},
		{
			addValidations: state => (validations = []) => {
				// Avoid re-adding of multiple used validations.
				const updates = validations.filter(({key, __id}) =>
					__id || !state.validations.find(val =>  val.key === key)
				);

				return updates.length ?
					{...state, validations: state.validations.concat(updates)} :
					state;
			},

			removeValidations: state => (keys = [], id) => {
				// This solves removing of multiplier-items for the moment.
				// Other FormFields will be ignored and always become validated.
				// Should not be a problem, if form is structured well.
				if (!id) {
					return state;
				}

				const updates = state.validations
					.map(validation => {
						const {key, __id} = validation;

						return keys.indexOf(key) >= 0 && __id === id ? {
							...validation,
							disabled: true
						} :
						validation
					});

				return {...state, validations: updates};
			},

			update: state => update => merge({}, state, update),
		}
	),

	getContext({
		findValue: PropTypes.func.isRequired,
		getValue: PropTypes.func.isRequired,
		name: PropTypes.string.isRequired,
	}),

	withHandlers({
		validate: props => (names = [], callback, debug = false) => {
			const {isValidating, RULES, validations, update, findValue, name} = props;

			new Promise(resolve => {
				const updatedValidations = validations
					.filter(validation => !validation.disabled)
					.map(validation => {
						const {names, tests, __id} = validation;

						const values = names.length === 1 ?
							findValue(names[0], __id) :
							names.reduce((valued, name) => ({
								...valued,
								[name]: findValue(name)
							}), {});

						return {
							...validation,
							isValid: checkIsValid({values, tests, RULES, debug, names}),
							isValidated: true
						};
					});

				resolve(updatedValidations);
			}).then(validations => {
				// Overall state of current ValidationProvider.
				const isValid = !validations.length || validations.every(check => check.isValid);

				update({
					validations,
					isValid,
				});

				// Inform asking instance by results.
				callback && callback.call(null, isValid);
			});
		},

		getValidationResults: props => (keys = [], id) => keys.reduce((results, key) => {
			const {isValid, isValidated} = props.validations.find(validation =>
				validation.key === key && (!id || validation.__id === id)) || {};

			return {
				...results,
				[key]: {isValid, isValidated}
			};
		}, {}),
	}),

	expose(API),

	mapProps(({addValidations, removeValidations, validate, validations, getValidationResults, RULES, update, name, ...props}) => props),
);
