import {debug, compose, defaultProps, getContext, isString, mapProps, merge, setPropTypes, withHandlers, withProps} from '../../utils';
import {expose} from '../utils';

import {shouldUpdate, shallowEqual} from 'recompose';

import React from 'react';
import PropTypes from 'prop-types';

const toMap = data => {
	switch (true) {
		case isString(data):
			data = [data];
		case Array.isArray(data):
			data = data.reduce((obj, key) =>
				({ ...obj, [key]: key }),
				Object.create(null)
			);
			break;
		default:
			break;
	}

	return data;
};

export default compose(
	getContext({
		findValue: PropTypes.func.isRequired,
	}),

	setPropTypes({
		names: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.array,
			PropTypes.object,
		]),
	}),

	defaultProps({
		values: {},
		names: [],
	}),

	withProps(props => ({
		values: merge({}, Object.entries(toMap(props.names)).reduce(
			(values, [newName, name]) => {
				const value = props.findValue(name);

				return value !== undefined ?
					{
						...values,
						[newName]: value
					} :
					values;
			}, Object.create(null)
		)),
	})),

	mapProps(({findValue, ...props}) => props),
);
