import {debug, compose, get, getContext, lifecycle, mapProps, set, withProps, withPropsOnChange, withStateHandlers} from '../../utils';
import {expose} from '../utils';

import {branch, renderNothing} from '../../utils';

import PropTypes from 'prop-types';

const API = {
	submit: PropTypes.func,
	nextPage: PropTypes.func,
	prevPage: PropTypes.func,
	toPage: PropTypes.func,
	hasPrevPage: PropTypes.bool,
	hasNextPage: PropTypes.bool,
};

export default compose(
	getContext(API),

	getContext({
		addPage: PropTypes.func,
		isActivePage: PropTypes.func,
		getPages: PropTypes.func,
		validate: PropTypes.func,
		name: PropTypes.string,
	}),

	withStateHandlers(
		{
			index: null
		},
		{
			updateIndex: state => index => set(state, 'index', index)
		},
	),

	lifecycle({
		componentDidMount() {
			const {name, title, updateIndex: callback} = this.props;

			// TODO: find a way to validate even on PageProgress
			this.props.addPage({name, title, callback});
		}
	}),

	branch(
		props => props.index === null,
		renderNothing,
	),

	withProps(props => {
		let enhancedProps = {};

		if (!props.dontValidate) {
			const withValidationBefore = handler => () => {
				props.validate(null, isValid => isValid && handler.call(null));
			}

			enhancedProps = {
				nextPage: withValidationBefore(props.nextPage),
				prevPage: withValidationBefore(props.prevPage),
				toPage: withValidationBefore(props.toPage),
			};
		}

		return {
			hasPrevPage: props.index > 0,
			hasNextPage: props.index < (props.getPages().pages.length - 1),
			isActive: props.index !== undefined && props.isActivePage(props.index),
			submit: () => props.validate(null, isValid => isValid && props.nextPage()),
			...enhancedProps,
		};
	}),

	expose(API),

	mapProps(({addPage, name, nextPage, prevPage, toPage, isActivePage, getPages, validate, submit, pages, currentPage, updateIndex, ...props}) => props),
);
