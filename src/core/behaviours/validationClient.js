import {debug, branch, compose, defaultProps, ensureArray, get, getContext, isPlainObject, lifecycle, mapProps, renderNothing, set, withHandlers, withProps} from '../../utils';
import withValues from './withValues';

import React from 'react';
import PropTypes from 'prop-types';

const mapAliasesToTests = (props = {}, aliases = []) =>
	aliases.filter(alias => props.hasOwnProperty(alias));

const mapPropsToValidations = (props = {}) => {
	let validations = [];

	// First get names/fields, we want to validate by current component.
	// Could be set by "names"-prop or by keys of a map for "rules"-prop.
	// Validation-Test will get all names as params for combined validations.
	let names = ensureArray(get(props, 'names', []));

	if (names.length) {
		let tests = [
			...(Array.isArray(props.rules) ? props.rules : []),
			...mapAliasesToTests(props, Object.keys(props.RULES)),
		];

		validations.push({
			names,
			tests: tests.length ? tests : ['required'],
		});
	}

	if (isPlainObject(props.rules)) {
		Object.entries(props.rules).forEach(([name, tests]) =>
			validations.push({
				names: name,
				tests: ensureArray(tests),
			})
		);
	}

	validations = validations.map(validation => ({
		...validation,
		key: validation.names.concat(validation.tests).sort().join('_'),
	}));

	return {validations};
}

const hasToBeRendered = args => {
	const {ifValid, ifNotValid, ifValidated, ifNotValidated, isValid, isValidated} = args;

	return (
		// show if not validated
		(ifNotValidated && !isValidated) ||
		// show if validated, result doesn't matter
		(ifValidated === true && isValidated) ||
		(
			!ifNotValidated && isValidated && (
				 // default: show, if invalid
				(!ifValid && ifNotValid && !isValid)  ||
				// negate: show, if valid
				(ifValid && isValid)
			)
		)
	);
};

export default compose(
	// Get requirements from Context.
	getContext({
		addValidations: PropTypes.func,
		removeValidations: PropTypes.func,
		getValidationResults: PropTypes.func,
		RULES: PropTypes.object,
	}),

	withProps(mapPropsToValidations),

	withProps(props => ({
		keys: props.validations.map(({key}) => key),
	})),

	lifecycle({
		// On mount register own validation on closest Validator-Store.
		componentDidMount() {
			this.props.addValidations(this.props.validations);
		},

		componentWillUnmount() {
			this.props.removeValidations(this.props.keys);
		}
	}),

	// Get valid-state for current component.
	withProps(props => {
		const results = props.getValidationResults(props.keys)

		return {
			isValid: Object.values(results).every(({isValid}) => Boolean(isValid)),
			isValidated: Object.values(results).every(({isValidated}) => Boolean(isValidated)),
		};
	}),

	defaultProps({
		ifNotValid: true
	}),

	// Always render custom renderer (by renderProp).
	// Will decide by itself, what and when to render.
	// Otherwise use logic from hasToBeRendered-helper above.
	branch(
		props => !props.render && !hasToBeRendered(props),
		renderNothing,
	),

	mapProps(({getValidationResults, addValidations, removeValidations, findValue, validations, rules, update, isValidation, RULES, ...props}) => props),
);
