import {debug, branch, compose, get, getContext, mapProps, renderNothing, withProps} from '../utils';
import {expose} from './utils';

import {
	multiplierProvider,
	pageClient,
	pageProvider,
	validationClient,
	validationProvider,
	valueProvider,
	valueClient,
	withSubmitHandler,
} from './behaviours';

import {getKnownComponent, isKnownComponent} from './utils';

import React from 'react';
import PropTypes from 'prop-types';

import {VALIDATION_RULES} from './constants';

// (Common data) => (Component-specific data) => (Component itself) => (Composition)
export default ({aliases = {}, config = {}}) => ({...data}) => Component =>
	compose(
		// Lookup for valueProvider in Context.
		// No valueProvider means, we are at root-level and have to add one.
		getContext({
			hasValueProvider: PropTypes.bool // => Detect ROOT-node.
		}),

		// Initial props-Setup. Keeps e.g. "name" from generic Components.
		withProps(props => ({
			...(config.inject || {}),
			...data,
			...props,
			isRoot: !props.hasValueProvider,
		})),

		// Ensure useful name for the current FormField.
		branch(
			props => !props.name || props.name === 'Component',
			renderNothing,
		),

		// Remove "name" from passed props. Get it from context instead.
		expose({
			name: PropTypes.string
		}),

		mapProps(({name, ...props}) => props),

		// Add valueProvider.
		branch(
			props => props.isValueProvider || props.isRoot,
			valueProvider,
		),

		// Each FormField always consumes values.
		valueClient,

		branch(
			props => props.isMultiplier,
			multiplierProvider,
		),

		branch(
			props => props.isValidationProvider || props.onSubmit || props.isPage,
			validationProvider({
				...VALIDATION_RULES,
				...get(config, 'validationRules', {})
			}),
		),

		branch(
			props => props.isValidation,
			validationClient,
		),

		branch(
			props => props.onSubmit || props.isPage,
			withSubmitHandler,
		),

		branch(
			props => props.isRoot,
			pageProvider,
		),

		branch(
			props => props.isPage,
			pageClient,
		),

		// Pushed to template at that point.
		getContext({
			name: PropTypes.string,
		}),

		// Wrap by (optional) template.
		branch(
			() => Boolean(config.template),
			Component => props =>
				React.createElement(
					config.template,
					props,
					React.createElement(Component, props)
				),
		),

		// Check for alias within props and maybe deliver built-in component.
		branch(
			isKnownComponent(Object.keys(aliases)),
			getKnownComponent(aliases)
		),

		// Remove some helper-props, not used at components.
		mapProps(({isRoot, isValidation, name, ...props}) => props),
	)(Component);
