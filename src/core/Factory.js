import {compose, get} from '../utils';
import ChildRenderer from './ChildRenderer';
import FormField from './FormField';

const Factory = ({aliases = {}, components = {}}) => (config = {}) => {
	const ConfiguredFormField = FormField({aliases, config});

	return new Proxy(components, {
		get: (obj, name) => {
			switch (name) {
				// Standalone Components. Just consumes APIs of FormField.
				case 'Remover': return obj.Remover;
				case 'Valued': return obj.Valued;
				default: break;
			}

			const Component = Reflect.get(obj, name) || ChildRenderer;

			return ConfiguredFormField({
				// Add (optional) defaultParams, e.g. to control behaviour.
				...get(Component, 'config', {}),
				name
			})(Component);
		}
	});
};

export default Factory;
