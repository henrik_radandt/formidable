import {compose, getContext} from '../utils';

import React from 'react';
import PropTypes from 'prop-types';
import isString from 'lodash.isstring';

const renderChild = ({injectedProps = {}, props = {}}) => child => {
	// Don't pass name to children, to give them a chance, to get an own name :-)
	const {name, ...childProps} = {...props, ...injectedProps, ...child.props};

	return isString(child.type) ?
		child :
		React.cloneElement(child, childProps);
};

const ChildRenderer = ({children, injectedProps, ...props}) =>
	children ?
		React.Children.map(children, renderChild({injectedProps, props})) :
		null;

ChildRenderer.defaultProps = {
	children: []
};

export default compose(
	// Inject helper from context as explicite props to each child.
	getContext({
		getValue: PropTypes.func,
		update: PropTypes.func,
	})
)(ChildRenderer);
