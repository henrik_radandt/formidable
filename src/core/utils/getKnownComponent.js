import {branch} from '../../utils';
import React from 'react';

const getAliasInPropNames = ({aliases, props}) =>
	Object.keys(aliases).find(alias => props.hasOwnProperty(alias));

export default (aliases = {}) => () => props => {
	const aliasName = props.is || getAliasInPropNames({aliases, props});
	return React.createElement(aliases[aliasName], props);
};
