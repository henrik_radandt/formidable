import expose from './expose';
import getKnownComponent from './getKnownComponent';
import isKnownComponent from './isKnownComponent';

export {
	expose,
	getKnownComponent,
	isKnownComponent
}
