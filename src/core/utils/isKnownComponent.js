const isKnownComponent = (aliases = []) => props =>
	aliases.length && (
		props.is && aliases.includes(props.is) ||
		aliases.some(prop => prop in props)
	);

export default isKnownComponent;
