import {pick, withContext} from '../../utils';

const expose = (apiPropTypes, map = () => ({})) => withContext(
  apiPropTypes,
  props => ({
    ...pick(props, Object.keys(apiPropTypes)),
    ...map(props)
  })
);

export default expose;
