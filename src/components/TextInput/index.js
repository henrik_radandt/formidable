import {debug, defaultProps, compose, getContext, onlyUpdateForKeys, setPropTypes, withProps} from '../../utils';
import React from 'react';
import PropTypes from 'prop-types';

const TextInput = props => {
	const {
		onChange,
		onBlur,
		value,
		placeholder,
		className,
		textarea,
		format
	} = props;

	const inject = {className, onChange, onBlur, placeholder, value};

	return textarea ?
		<textarea {...inject}/> :
		<input type={'text'} {...inject}/>
};

export default compose(
	getContext({
		getValue: PropTypes.func,
		update: PropTypes.func,
	}),

	withProps(props => ({
		onChange: e => props.update(e.target.value),
		value: props.getValue(),
	})),

	withProps(props => {
		return props.format ?
			props.format(props) :
			{}
	}),

	defaultProps({
		value: '',
		className: '',
		placeholder: '',
		textarea: false,
	}),

	setPropTypes({
		value: PropTypes.string,
		className: PropTypes.string,
		placeholder: PropTypes.string,
		format: PropTypes.func,
		textarea: PropTypes.bool,
	}),

	onlyUpdateForKeys(['value']),
)(TextInput);
