import {branch, compose, defaultProps, getContext, renderNothing, withProps} from '../../utils';

import React from 'react';
import PropTypes from 'prop-types';

// TODO: Accept children for ChildRenderer.
const PageProgress = ({pages, render, className = ''}) => (
	<div className={className}>
		{pages.map(render)}
	</div>
);

export default compose(
	getContext({
		getPages: PropTypes.func,
		toPage: PropTypes.func,
	}),

	defaultProps({
		pages: [],
	}),

	withProps(props => {
		const {pages, current} = props.getPages();

		return {
			pages: pages.map((page, index) => ({
				...page,
				key: index,
				index,
				isActive: current === index,
				toPage: props.toPage
			}))
		};
	}),

	branch(
		props => !props.pages.length,
		renderNothing
	),
)(PageProgress);
