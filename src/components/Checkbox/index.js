import {compose, defaultProps, getContext, onlyUpdateForKeys, setPropTypes, withProps} from '../../utils';
import React from 'react';
import PropTypes from 'prop-types';

const Checkbox = ({onChange, checked}) => (
	<input type={'checkbox'} {...{onChange, checked}}/>
);

export default compose(
	getContext({
		getValue: PropTypes.func,
		update: PropTypes.func,
	}),

	withProps(({update, value}) => ({
		checked: Boolean(props.getValue()),
		onChange: e => update(e.target.checked),
	})),

	defaultProps({
		checked: false,
	}),

	setPropTypes({
		checked: PropTypes.bool,
	}),

	onlyUpdateForKeys(['checked']),
)(Checkbox);
