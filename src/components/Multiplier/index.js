import {debug, defaultProps, compose, ensureArray, getContext, lifecycle, mapProps, merge, renameProp, withProps, withHandlers} from '../../utils';
import {expose} from '../../core/utils';
import MultiplierItem from './MultiplierItem';

import {ChildRenderer} from '../../core';

import React from 'react';
import PropTypes from 'prop-types';

const ListRenderer = ({children, items, add, remove, min, max, className, name}) => {
	return Array.isArray(items) ?
		items.map(item => (
			<div className={className} key={item.__id}>
				{
					React.createElement(MultiplierItem, {
						...item,
						children,
						add,
						remove: (...args) => remove(item.__id, ...args),
						min,
						max,
						items
					})
				}
			</div>
		)) :
		null;
};

const withMultiplierProps = compose(
	getContext({
		getValue: PropTypes.func.isRequired,
		add: PropTypes.func,
		remove: PropTypes.func,
	}),

	withProps(props => ({
		items: props.getValue(),
	})),
);

const Remover = ({render, ...props}) => {
	return render ? render(props) : React.createElement(ChildRenderer, props);
}

const Adder = withMultiplierProps(Remover);
const List = withMultiplierProps(ListRenderer);
export {Adder, Remover};
export default List;
