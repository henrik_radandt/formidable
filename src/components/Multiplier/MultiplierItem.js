import {debug, compose, getContext, mapProps, withProps} from '../../utils';
import {expose} from '../../core/utils';
import {ChildRenderer} from '../../core';

import React from 'react';
import PropTypes from 'prop-types';

const API = {
	update: PropTypes.func,
	getValue: PropTypes.func,
	findValue: PropTypes.func,
	addValidations: PropTypes.func,
	removeValidations: PropTypes.func,
	getValidationResults: PropTypes.func,
	name: PropTypes.string.isRequired,
};

export default compose(
	getContext(API),

	withProps(props => {
		return {
			update: value => props.update(value, props.__id),

			getValue: () => props.getValue().find(({__id}) => __id === props.__id),

			addValidations: (validations = []) => {
				const updatedValidations = validations.map(validation => ({
					...validation,
					__id: props.__id
				}));

				props.addValidations(updatedValidations);
			},

			removeValidations: keys =>
				props.removeValidations(keys, props.__id),

			getValidationResults: keys =>
				props.getValidationResults(keys, props.__id),

		};
	}),

	expose(API),
)(ChildRenderer);
