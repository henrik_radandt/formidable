import {withValues} from '../../core/behaviours';
import {ChildRenderer} from '../../core';
import {branch, clean, compose, defaultProps, getContext, renderNothing, setPropTypes, withProps} from '../../utils';

import React from 'react';
import PropTypes from 'prop-types';

const hasToBeRendered = props => {
	const {ifSubmitted, ifNotSubmitted, ifSubmitting, ifNotSubmitting, isSubmitting, isSubmitted} = props;

	return (
		(ifSubmitted === undefined && ifNotSubmitted === undefined && ifSubmitting === undefined) || (
			(ifSubmitted === undefined && ifNotSubmitted === undefined) ||
			(ifSubmitted === true && isSubmitted === true) ||
			(ifNotSubmitted === true && isSubmitted === false)
		) && (
			(ifSubmitting === undefined && ifNotSubmitting === undefined) ||
			(ifSubmitting === true && isSubmitting === true) ||
			(ifNotSubmitting === true && isSubmitting === false)
		)
	);
};

const Valued = ({children, render, values, ...inject}) => {
	return render ?
		render({values, ...inject}) :
		React.createElement(ChildRenderer, {children, values, ...inject});
}
export default compose(
	getContext({
		cleaner: PropTypes.object,
	}),

	withValues,

	defaultProps({
		values: {},
		cleaner: {},
	}),

	withProps(props => ({
		values: props.clean ? clean(props.values, props.cleaner) : props.values
	})),

	getContext({
		isSubmitting: PropTypes.bool,
		isSubmitted: PropTypes.bool,
	}),

	branch(
		props => !hasToBeRendered(props),
		renderNothing,
	),

	setPropTypes({
		render: PropTypes.func,
		values: PropTypes.object.isRequired,
	}),
)(Valued);
