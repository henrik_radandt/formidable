import React from 'react';
import PropTypes from 'prop-types';

import {branch, compose, defaultProps, getContext, renderNothing, withProps} from '../../utils';

const Submit = ({submit, children, className}) => {
	return (
		<div onClick={submit} className={className}>
			{children}
		</div>
	);
};

export default compose(
	getContext({
		submit: PropTypes.func,
		getPages: PropTypes.func,
	}),

	branch(
		props => !props.submit,
		renderNothing,
	),

	withProps(props => {
		const {pages, current} = props.getPages();

		return {
			toRender: !props.atLastPage || current === (pages.length - 1),
			submit: e => {
				e.preventDefault();
				e.stopPropagation;

				props.submit();
			}
		};
	}),

	branch(
		props => !props.toRender,
		renderNothing
	),

	defaultProps({
		className: '',
	}),
)(Submit)
