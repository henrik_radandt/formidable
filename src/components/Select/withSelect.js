import {compose, defaultProps, get, getContext, onlyUpdateForKeys, withProps} from '../../utils';

import React from 'react';
import PropTypes from 'prop-types';

export default compose(
	getContext({
		getValue: PropTypes.func,
		update: PropTypes.func,
	}),

	withProps(({update, getValue}) => ({
		value: getValue(),
		onChange: e => update(e.target.value),
	})),

	defaultProps({
		className: '',
	}),

	onlyUpdateForKeys(['value']),
);
