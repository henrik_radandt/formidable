import withSelect from './withSelect';
import {compose, lifecycle} from '../../utils';
import React from 'react';
import PropTypes from 'prop-types';

const Option = ({value, label}) => <option key={value} value={value}>{label || value}</option>;

const Select = ({
	onChange,
	value,
	options,
	children,
	className
}) => (
	<select {...{onChange, value, className}}>
		{options && options.map(Option)}
		{!options && children}
	</select>
);

export default compose(
	withSelect,

	lifecycle({
		componentDidMount() {
			const {value, options, children, update} = this.props;

			if (value === undefined) {
				try {
					update(get(React.Children.toArray(children), '[0].props.options[0].value', ''))
				} catch(e) {}
			}
		}
	}),
)(Select);
