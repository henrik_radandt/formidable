import {expose} from '../../core/utils';
import {branch, getContext, renderNothing} from '../../utils';
import {ChildRenderer} from '../../core';

import React from 'react';
import PropTypes from 'prop-types';

const Page = props => {
	const {isActive, children, render, ...rest} = props;

	return render ?
		render({...rest, children}) :
		React.createElement(ChildRenderer, {children, ...rest});
};

export default branch(
	props => props.index === undefined || !props.isActive,
	renderNothing,

	getContext({
		name: PropTypes.string,
		hasPrevPage: PropTypes.bool,
		hasNextPage: PropTypes.bool,
		prevPage: PropTypes.func,
		nextPage: PropTypes.func,
		toPage: PropTypes.func,
		getPages: PropTypes.func,
	}),
)(Page);
