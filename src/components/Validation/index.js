import {ChildRenderer} from '../../core';
import React from 'react';

const Validation = ({render, children, ...props}) =>
	render ?
		render({children, ...props}) :
		React.createElement(ChildRenderer, {children, ...props});

export default Validation;
