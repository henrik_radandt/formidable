import Checkbox from './Checkbox';
import Multiplier, {Adder, Remover} from './Multiplier';
import Page from './Page';
import PageProgress from './PageProgress';
import Select from './Select';
import Submit from './Submit';
import TextInput from './TextInput';
import Validation from './Validation';
import Valued from './Valued';

const ALIASES = {
	check: Checkbox,
	checkbox: Checkbox,
	input: TextInput,
	select: Select,
	text: TextInput,
};

Validation.config = {isValidation: true};
Page.config = {isPage: true};
Multiplier.config = {isMultiplier: true};
Adder.config = {isMultiplier: true};

export {
	ALIASES,

	Adder,
	Remover,
	Checkbox,
	Multiplier,
	Page,
	PageProgress,
	Select,
	Submit,
	TextInput,
	Validation,
	Valued,
};
