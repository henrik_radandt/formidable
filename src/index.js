import * as componentLib from './components';
import {Factory} from './core';

const {ALIASES = {}, ...components} = componentLib;

export default Factory({components, aliases: ALIASES});
