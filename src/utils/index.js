import clean from './clean';
import debug from './debug';
import ensureArray from './ensureArray';

import {
	branch,
	compose,
	defaultProps,
	getContext,
	lifecycle,
	mapProps,
	onlyUpdateForKeys,
	renameProp,
	renderNothing,
	setPropTypes,
	shallowEqual,
	shouldUpdate,
	withContext,
	withHandlers,
	withProps,
	withPropsOnChange,
	withStateHandlers
} from 'recompose';

import get from 'lodash.get';
import isPlainObject from 'lodash.isplainobject';
import isString from 'lodash.isstring';
import merge from 'lodash.merge';
import mergeWith from 'lodash.mergewith';
import pick from 'lodash.pick';
import set from 'lodash.set';

export {
	clean,
	debug,
	ensureArray,

	// Lodash helper.
	get,
	isPlainObject,
	isString,
	merge,
	mergeWith,
	pick,
	set,
	
	// Recompose helper.
	branch,
	compose,
	defaultProps,
	getContext,
	lifecycle,
	mapProps,
	onlyUpdateForKeys,
	renameProp,
	renderNothing,
	setPropTypes,
	shallowEqual,
	shouldUpdate,
	withContext,
	withHandlers,
	withProps,
	withPropsOnChange,
	withStateHandlers,
};
