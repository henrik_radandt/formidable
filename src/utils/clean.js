const REMOVE = ['__id'];
const RENAME = {};

const clean = (values, cleaner) =>
	Object.entries(values).reduce((cleaned, [key, value]) => {
		let updateKey = cleaner.rename[key] || key;
		let updateValue = value;

		switch (true) {
			case (cleaner.remove.indexOf(key) >= 0):
				return cleaned;

			case (Array.isArray(updateValue)):
				return {
					...cleaned,
					[updateKey]: updateValue.map(v => clean(v, cleaner))
				};

			case (updateValue === Object(updateValue)):
				return {
					...cleaned,
					[updateKey]: clean(updateValue, cleaner)
				};

			case (typeof updateValue === 'boolean'):
				updateValue = updateValue === true ? 'Ja' : 'Nein';
				break;

			default:
				break;
		}

		return {
			...cleaned,
			[updateKey]: updateValue
		};
	}, {});

export default (data = {}, customCleaner = {}) => {
	const {rename = {}, remove = []} = customCleaner;

	const cleaner = {
		remove: [...REMOVE, ...remove],
		rename: {...RENAME, ...rename},
	};

	return clean(data, cleaner);
};
