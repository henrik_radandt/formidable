import React from 'react';

export default (name, ...propNames) => Component => props => {
	if (propNames && propNames.length) {
		console.log(name, propNames.reduce((all, name) => ({...all, [name]: props[name]}), {}));
	} else {
		console.log(name, props);
	}

	return React.createElement(Component, props);
};
