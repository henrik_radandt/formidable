# formidable

A JS-lib to build up forms easy, fast and readable, but also support typical non-native features to create more complex user-experiences.

## Before we start
This lib uses context-feature of the React-lib extensivily, if you not like this often discussed technology, please don't use this lib.

Also it is a try for an abstraction of a form-building-tool and for sure not the first, the last, certainly not the best one.
But hey, I'm a developer and I learned a lot by implementing it.

Also it was created parallel to a concrete project and so there should be a ton of more use-cases, the lib should cover.
Please feel free to comment, send issues or ask questions about it.

## Features / UseCases (t.b.c.)

* Easy setup of common (native) Form-Elements.
* Use templates to ease layouting.
* Many built-in components to shorten common Form-practises:
  * Validation
  * Pagination/Subforms
  * SubmitHandler
  * Dynamic Form-Parts, e.g. multiplying of Snippets
  * Input-formats
  * Data-Cleaning
  * ...

## Concept

formidable just delivers a configurable factory. Each component (built-in or custom) will be wrapped in a setup of behaviours, I called it "FormField".  
Behaviours will differ, depending of special props, attributes or requirement.

By creating the form, internally a data-structure (tree) will be updated on every change immediatly.

### Data Storage

There automatically will be added a data-store (value-provider) at the  root-level, which should be the most common use-case.  
Each child will "bubble" data to closest FormField-parent and so on.  
This allows modifying of data or even semi-independent subforms by adding more value-provider at deeper points.

### Validation

Validations are not bound to the input, the data came from. This allows many more options to validate more complex use-cases. E.g. you can collect values of
more than one input and handle switches, depending on multiple conditions.  
Also styling, layouting and dynamic validations are easier to setup.

Validations are stored similar to input-data in a validation-provider, one is created automatically at root level.  
Pages also always will be a validation-provider, so each page can be validated independently from the rest of the form (or other pages).

### Pages/Subforms

Pages works as a subform (or "form-in-a-form"). They act and will be validated for its own, but by default they also will commit their values to the root-store.

Subforms, e.g. maybe a remote check of a voucher-code, can be created easily. That is not possible/valid at native forms.

### ...

## Requires
- react
- lodash
- recompose
- prop-types

## Install

=> Missing publication at npm.

## Init a formidable-factory

```js
import Formidable from 'formidable';

const factory = new Formidable();
```

There are some options, you can set at initialization, e.g. Template-Components, additional custom validation-rules ...
See more below.

```js
import MyTemplate from './MyTemplate';
import styles from './MyStyles.json';

// A template will be wrapped around each component, build by this factory.
// Each component also has access to the given classNames from the styles.json.
const factory = new Formidable({
    template: MyTemplate,
    inject: {styles}
});
```

## Access/Build formidable-components

There are two types of components you can use within a formidable-form:
* build-in components, e.g. TextInput, Submit, Validation, Page ...
* custom components, that will be wrapped by the FormField-behaviours (API)

You can access built-in components by explicite name of the component. Nothing special here.

```js
const {TextInput, Submit, Validation} = new Factory();
```

Custom components differ a little bit. There is a handy shortcut, that will give you a very special way to build forms.
You build them explicit by the name, under which the data will be stored later.

```js
const {Contact, Firstname, Lastname, Email, Newsletter} = new Factory();

const Form = (
  <Contact>
    <FirstName text/>
    <LastName text/>
    <Email is="text"/>
    <Newsletter check/>
  </Contact>
);
```

By that way you easy can capture the (nested) data-structure of the submitted data.
Names will be case-sensitive, so you can use both ways.

// TODO: enable lowercase names

As you see, the types of the inputs will be defined by an alias or explicite set by the prop `is`.  
Read more below.

On the other side, you also can build the form along the structure of the inputs.

```js
const {TextInput, Checkbox, Contact} = new Factory();

const Form = (
  <Contact>
    <TextInput name="FirstName"/>
    <TextInput name="LastName"/>
    <TextInput name="Email"/>
    <Checkbox name="Newsletter"/>
  </Contact>
);
```

You also can access your components by the factory directly in JSX.

```js
const F = new Factory();
const Email = F.Email;

// Confusing mix ;-)
const Form = (
  <F.Contact>
    <F.TextInput name="FirstName"/>
    <F.LastName text/>
    <Email is="text"/>
    <F.Checkbox name="Newsletter"/>
  </F.Contact>
);
```

**Hint:**
By using the prop `name` (and only that way), you are also allowed to use more unconventional strings for names, e.g. containing spaces ones.
The keys will be used also for submitted datas and maybe can reduce a lot of work on serverside transformations.

```js
<Checkbox name="I want to recieve the newsletter."/>;
```

## Built-in Input-Components

There is a set of built-in components, most of them renders the corresponding native form-element,
embedded in the FormField-behaviours, they require.

For the moment there are:
// TODO: say something about their APIs

* TextInput (alias "text" or "input"), a native text-input
* Checkbox (alias "check"), a native check-box, which children will also be be rendered (e.g. for use as label).
* Select (alias "select"), a native select, options will be injected by prop `options` or by children.

* [TextArea]: You will get a textarea by using just a TextInput with prop `textarea` set.

## Initial Values:

You can simple set an inital value by prop `value`.

```js
const {Salutation} = new Factory();

const SalutationSelect = (
  <Salutation select value="Mrs.">
    <option value="Mr.">Mr.</option>
    <option value="Ms.">Ms.</option>
    <option value="Mrs.">Mrs.</option>
  </Salutation>
);
```

**Recipe:**  
By default, the values of inputs always will be stored the first time onChange to the closest value-provider.  
Not entering a value to an input will skip this name at the submitted data.
To ensure all names in dataset, even if value is empty, set initial value e.g. to an empty string for TextInputs.

## Submit a form

There is no behaviour defined at the moment. Each FormField puts values to the parent one and so on. At root this will stop.

### Submit-Handler

To work with data at root (or each other useful point within tree), just add prop `onSubmit`.

```js
const onSubmit = data => console.log('This we have', data);

const Form = (
  <MyForm onSubmit={onSubmit}>
    {/* ... */}
  </MyForm>
);
```

### Submit-Trigger
There is a built-in component `Submit`. It will automatically trigger the closest Submit-Handler.  
Submit-Handling always will execute validation (also of the closest validation-provider) and depending on result,   
onSubmit-Handler will be executed or not.

```js
const {Submit} = new Factory();

const Form = (
  <MyForm onSubmit={onSubmit}>
    <Submit>
		<span>SAVE</span>
	</Submit>
  </MyForm>
);
```

Each `Page` is a subform with an own internal onSubmit-Handler. So each page is validated for its own.  
Putting a `Submit` inside a `Page` will validate it and if page is valid, jump to the next page. There are more options, see Pages.

### Submitted data

To manipulate data BEFORE access them by the onSubmit-Handler, you can add cleaner to the component.  
Internally there are already cleaner at work, e.g. to hide keys like `__id` used by array-values from output.  
There are two fields to work on data:

* rename: add a map of names, that should have another name on output
* remove: array of keys, you won't see in output

**Recipe:**
If you want to add (pass through) data, in native forms often done by hidden inputs, just put them in `value`-prop, to store them initial.

## Accessing data

There is a very special component, called "Valued". It will give you read-access to the data-providers and also delivers useful data about the form-state.

### Read data

Let's explain by example.

```js
const {Valued} = new Factory();
const F = new Factory();

const CityInfo = ({values: {City}}) =>
    <div>{`${City} is a nice place to live.`}</div>;

const Form = (
  <F.Address>
    <F.Street text/>
    <F.Zip text/>
    <F.City text/>

    <Valued names="City" render={CityInfo}/>
  <F.Address>
);

```

The Valued-Component just wants to know which values (prop `names`) to grab and where to inject.

`names` should be the existing name or an array of names somewhere at the form. It's important, where you put the Valued-Component within the form.  
It will walk from it's own place bottom up the FormField-tree to find a value, matching the requestes name(s).  
To access values in another branch, you have to extend the path of the name to match it.


```js
const {Valued} = new Factory();
const F = new Factory();

const CityPersonInfo = ({values: {City, ['PersonFirstName']: FirstName}}) =>
    <div>{`Hi {FirstName}. ${City} is a nice place to live.`}</div>;

const Form = (
  <F.UserInfo>
    <F.Person>
        <F.FirstName text>
        <F.LastName text>
    </F.Person>
    <F.Address>
        <F.Street text/>
        <F.Zip text/>
        <F.City text/>

        <Valued names={["City", "Person.FirstName"]} render={CityPersonInfo}/>
    <F.Address>
  </F.UserInfo>
);

```

There is a way to rename the names (for obvious reasons) you're looking for. Just use a map.

```js
const {Valued} = new Factory();
const F = new Factory();

const CityPersonInfo = ({values: {city, firstname}}) =>
    <div>{`Hi {firstname}. ${city} is a nice place to live.`}</div>;

const Form = (
  <F.UserInfo>
    <F.Person>
        <F.FirstName text>
        <F.LastName text>
    </F.Person>
    <F.Address>
        <F.Street text/>
        <F.Zip text/>
        <F.City text/>

        <Valued names={{
         city: "City",
         firstname: "Person.FirstName"
        }} render={CityPersonInfo}/>
    <F.Address>
  </F.UserInfo>
);

```

'Valued' accepts a (preferred) render-prop by `render`, but also injects values to children.

```js
<Valued names="myName">
    <MyComponentThatWorksWithValues/>
</Valued>
```

### Cleaned Data

As described in part "Submit", there is a mechanism to transform (clean) data, before submitting it.  
If you already need the cleaned data (or a part of it) before submit, just add prop `clean` to a `Valued`-component.  
If cleaner are defined, they will be applied, before giving the data to the children.

**Recipe:**
I found that very helpful for rendering a generative summary of all form-data before submit.

## Read form-state

At the moment, there are two form-states, you can access by `Valued`.

* `ìsSubmitting`: The form (more accurate the closest submitHandler) is current sending it's data.
* `isSubmitted`: The form was submitted.

**Why is this useful?**

You can easily show e.g. a spinner, while submitting or a post-submit-view ("Thanks for giving us your data!").

To support this, `Valued` respects four props to control, when it will be rendered:

* `ifSubmitting` / `ìfNotSubmitting`
* `ifSubmitted` / `ìfNotSubmitted`

```js
    <Valued ifNotSubmitted ifNotSubmitting>
        <MyForm/>
    </Valued>
    <Valued ifSubmitting>
        <Spinner/>
    </Valued>
    <Valued ifSubmitted>
        <Thanks/>
    </Valued>
```

**Hints:**
Don't use them with a value "false", just set it.  
If you combine the props, it will be checked by "AND" for all of them. So using "ifSubmitting" and "ifSubmitted" at the same time, won't never show you a result.

## Validation

Validation is a really, really complex topic. Had fun and desparation likewise when implementing it.  
It was no option for me, to strongly couple validation with corresponding input, like WebAPI does.

So you will get a built-in `Validation`-component. Let's see, what we can do with it.

### Setup, what and how to test

```js
import myExternalTest from './myExternalTest';

const {Validation} = new Factory({
    validationRules: {
        myExternalTest,
        allowedDomains: value => {...}
    }
});
const F = new Factory();

const Form = (
  <F.UserInfo>
    <F.Person>
        <F.FirstName text>
        <Validation names="LastName">
            We need to know your name
        </Validation>
        <F.LastName text>

        <Validation names={['Email']} rules={['email', 'allowedDomains']}>
            We need to know your email
        </Validation>
        <F.Email text>
    </F.Person>
    <F.Address>
        <F.Street text/>
        <F.Zip text/>
        <F.City text/>
    <F.Address>

    <Validation names={['Address.Zip', 'Address.City']} rules={['myExternalTest']}>
        Sure, zip and city are correct?
    </Validation>
  </F.UserInfo>
);

```

First there is prop `names`. Can be a string or array of strings, describing, which fields you want to validate.  
Similar to `Valued`, you have to give a most sufficient path to find value(s). See the path from the point of the closest validation-provider.

Next prop is `rules`, also an array of strings, naming the tests, we will execute for validation. There are many built-in tests (see below) and if none is set,
`required` becomes the default.
There are also many shortcuts to setup rules, you want to validate. Just put them in as a prop. (`required`, `email`, ...)

To setup custom tests (or overwrite existing ones), put them in a map with key `validationRules` in factory-configuration.  
A test always get the requested values (as immediate param, if only one is requested and as object/map with requested keys for multiple values).  
Test have to return a boolean value, whether the value is valid (return `true`) or not (`false`).

// TODO: specify rules-building
You see, `names` and `rules` are combined to exactly one test, where alle values are given.

To setup multiple tests with several fields at the same `Validation`-component, give a map to `rules` and skip `names`.

### Rendering the results

By default `Validation` renders its children (or also a component defined by `render`), if the values were validated and at least one test fails.  
But you also can control rendering by multiple options:

* `ifValidated` (`ifNotValidated`): show content, if a validation was executed (or not), no matter, what the results are
* `ifValid` (`ifNotValid`): show content, if a validation was executed and the results are valid (not valid)

TODO: add ìfValidating for expensive tests

### Customize the render-decision

To get full control over the result or also to avoid multiple `Validation`-components for the same rules,  
you always have access to the current state-values:

* `isValidated`: current names-rules-set is validated
* `isValid`: current names-rules-set is validated and valid

**Hint:**
This just works with a component, given by render-prop `render`.

**Hint:**
I hardly recommend, to put an output for the same name-rule-set, but different validation-results into one `Validation`-component, than multiple ones.

```js
const {Validation} = new Factory();
const F = new Factory();

const LabelWithValidation = ({isValidated, isValid}) => {
    switch (true) {
        case isValid:
            return <label>Everything is fine.</label>;
        case (isValidated && !isValid):
            return <label>Something went wrong.</label>
        default:
            break;
    }
    return <label>Please enter something here</label>
};

const Email = (
    <div>
        <Validation names="Email" render={LabelWithValidation}/>
        <F.Email text>
    </div>
);

```

**Hint:**
Keep in mind, that a submit (or otherwise validation-trigger) always will just validate the `Validation`-components, belonging to the closest validation-provider.  
E.g. a `Page` won't be validated by a `Submit` outside of this page.

## Pages (and Subforms)

Pages by my definition are nothing else as a form within a form. They have an own validation-provider and pre-defined submit-handling (see above).  
A `Submit` within a `Page` will trigger the validation of that page and will switch to the next page, if valid.

Pages, should be created as siblings within a form. They will be stored in a page-provider at root level.
(TODO: think about nested pages)

There is a special helper `PageProgress`, that easily generates an overview over all pages.

Each FormField-child of a `Page` will get access to the following controls for their parent-page:

* `prevPage`/`nextPage`: call, to switch to prev/next page, if current one is valid
* `toPage`: call with an index (zero-based), to jump to the requested page
* `hasPrevPage`/`hasNextPage`: Boolean, to see, if there is a prev/next page
* `getPages`: call, to get an object with a field `pages`, containing infos about all pages and `current` as the current active page-index


```js
import Summary from './Summary';
const {Page, PageProgress, Submit, MyForm} = new Factory();

const PageProgressItem = ({title, name, isActive, index}) => (
    <div className={isActive ? 'active' : ''}>
        {`${index + 1}. ${title || name}}
    </div>
);

const Form = (
    <MyForm>
        <PageProgress render={PageProgressItem}/>

        <Page name="user" title="Userdata">
            {...}
            <Submit><span>Next page</span></Submit>
        </Page>

        <Page name="contact" dontValidate>
            {...}
            <Submit><span>Next page</span></Submit>
        </Page>

        <Page name="summary" render={Summary}/>

        <Submit atLastPage><span>Send</span></Submit>
    </div>
);

```

You see a few more special features here:

* A `Page` also accepts a render-prop `render`.
* To avoid a validation of the current page and always allow jumping to the next (or prev) page, add prop `dontValidate` to a `Page`.
* To enable an outer submit not before passed (and validated) all pages, add prop `atLastPage` to the `Submit`.  
It will appear, if user has reached the last page.

**Hint**
This is a more special usecase of the project, I worked for. The last page is a summary, so there are no validations on it and you can submit  
the root-form without any problems without "leaving" the last page.  
I soon will add an option, to enable appearing of submit, AFTER last page also was validated.


## Multiplier

A multiplier is a tool to create instances of a form-partial in any number. E.g. you want to add participants to a cruise or add waypoints to a route planner.

`Multiplier` is a built-in component and also offers two helper `Adder` and `Remover`. Let's see an example.

```js
const F = new Factory();
const {Multiplier, Adder, Remover} = F;

const RemoveMe = ({remove}) => <button onClick={remove}>Remove Contact</button>;
const AddOne = ({add}) => <button onClick={add}>One more</button>;

const Form = (
    <MyForm>
        <Multiplier name="contacts" min={1} max={100}>
            <F.name text/>
            <F.email text/>
            <F.mobile text/>
            <Remover render={RemoveMe}/>
        </Multiplier>
        <Adder name="contacts" min={1} max={100} render={AddOne}/>
    </MyForm>
);

```

What to say here?
* Each `Multiplier` requires a `name`, to store values.
* You can limit the items by (optional) `min` and `max`. At the beginning, you will find `min` items pre-rendered and you wont't add more items than `max`.
* The children form the snippet, we want to multiply. Sorry, there is no other way at the moment, especially if you using more inputs inside a Multiplier.
* A `Remover` only make sense within the snippet, because of his binding to a conrete item.
* An `Adder` will stand outside of the `Multiplier` and therefore need the same configuration (still trying to find a better solution).
* Each child of a `Multiplier` will also have access to the props `add` and `remove` like `Adder` and `Remover`.
* Values will be stored in an array of objects, identified by an internal key `__id`. These identifier will be removed from submitted data.


## Configure the factory

### Injecting props

You can inject common props into each FormFields by factory-config.

```js
import buildFactory from 'formidable';
import styles from './styles.css.json';

const Factory = buildFactory({
	inject: {
		styles,
		createdAt: Date.now()
	}
});
```

**Hint:**  
Avoid prop-names used by formidable, e.g. 'name', 'names', 'validate' ... except you want set this props explicitly.

See another example at Template below.

### Templates

Templates are components, wrapped around the rendered FormField-component. Primarily you will use them for layouting and avoid redundant markup. It will uphold the readability of your form.

```js
import buildFactory from 'formidable';
import styles from './styles.css.json';
import {withProps} from 'recompose';

const Template = ({name, styles, children}) => (
	<div className={styles.input}>
		<label>{name}</label>
		{children}
	</div>
);

// Specialized factory.
const Text = buildFactory({
	inject: {
		text: true,
		value: ''
	},
	template: withProps({styles})(Template)
});

// Common factory.
const F = buildFactory();

const Form = (
	<F.MyForm>
		<Text.firstName/>
		<Text.lastName/>
		<Text.email/>
	<F.MyForm>
);
```

As you will spot, we created a factory, that just will produce `TextInput` (enforced by alias 'text').  
Also each build input has a default value, so it's key is ensured in submitted data, even if value never will be changed by user.
Each input is wrapped in defined template (enhanced by styles) and will be given to it by prop `children`. Prop `name` also is accessible for each template.

### Custom validation rules

You can provide custom validation-tests by config. They will be available by key in each `Validation`-component.

You can find an example at part 'Validation'.
