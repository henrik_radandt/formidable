const path = require('path');

module.exports = env => {
	return {
		mode: "development",
		entry: './src/index.js',
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: 'bundle.js',
			libraryTarget: 'umd',
			publicPath: '/dist/',
			umdNamedDefine: true
		},
		module: {
			rules: [
				{
					include: [path.resolve(__dirname, 'src')],
					exclude: /node_modules/,
					use: {
						loader: "babel-loader"
					},
				},
			]
		},
		resolve: {
			extensions: ['*', '.js'],
			alias: {
				'react': path.resolve(__dirname, './node_modules/react'),
			}
		},
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: 'bundle.js'
		},
		externals: {
			react: {
				commonjs2: "react",
				commonjs: "react",
				amd: "react",
				root: "react"
			}
		}
	};
};
