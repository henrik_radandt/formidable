import React from 'react';
import ReactDOM from 'react-dom';

import Factory from '../lib';

const Template = ({name, required, children}) => (
	<div>
		{required && <Validation names={[name]} required/>}
		<h2>{name}</h2>
		<div>{children}</div>
	</div>
);

const F = Factory({
	template: Template
});

const ValueRenderer = ({values}) => (
	<div>
		{Object.entries(values).map(([key, value]) => (
			<div key={key}>{key}: {value}</div>
		))}
	</div>
)

const {Validation, Valued} = Factory();

const App = () => (
	<F.newsletter /*onSubmit={console.log}*/>
		<Validation names={['user']}/>
		<F.user>
			<F.firstName text value={"Henrik"} required/>
			<F.lastName is="text" value="" required/>

			<F.contact>
				<F.mobile value="123" text required/>
				<F.fax text/>

				<Valued names={['mobile', 'fax']}>
					<ValueRenderer/>
				</Valued>
			</F.contact>

		</F.user>

		<hr/>

		<Validation names={['user.contact.mobile']} required/>

		<F.email input/>

		<F.agb check/>

		<Valued names={{firstName: 'user.firstName',lastName: 'user.lastName'}}>
			<ValueRenderer/>
		</Valued>

		<F.Submit/>
	</F.newsletter>
);

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);
